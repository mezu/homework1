package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        GenerateRandomNumber()
    }

    private fun GenerateRandomNumber(){

        Generate_Random_Button.setOnClickListener {
            val Randomnum = (-100..100).random()
            if ((Randomnum % 5 == 0) and (Randomnum > 0)){
                Random_Number.text = "$Randomnum Yes"
            }
            else {
                Random_Number.text = "$Randomnum No"
            }
        }

    }










}